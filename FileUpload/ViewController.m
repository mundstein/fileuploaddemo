//
//  ViewController.m
//  FileUpload
//
//  Created by Sascha Mundstein on 18/04/15.
//  Copyright (c) 2015 Sascha Mundstein. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UIImagePickerControllerDelegate>
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonTapped:(id)sender {

    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = (id)self;
    imagePicker.allowsEditing = YES;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:imagePicker animated:YES completion:NULL];

}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *image = info[UIImagePickerControllerEditedImage];
    [picker dismissViewControllerAnimated:YES completion:^{
        [self uploadImage:image];
    }];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(NSString *)randomTransactionID {
    NSString *letters = @"abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    NSMutableString *str = [NSMutableString string];
    int i = 0;
    NSInteger count = letters.length;
    while (i++ < 20) {
        [str appendFormat:@"%c", [letters characterAtIndex:arc4random() % count]];
    }
    return [NSString stringWithString:str];
}


-(void)uploadImage:(UIImage *)image {
    
    [self.webView loadHTMLString:@"" baseURL:nil];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    NSURL *url = [NSURL URLWithString:@"http://mundstein.at/upload/upload.php"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    request.timeoutInterval = 20;
    
    [params addEntriesFromDictionary:@{
                                       @"client_id" : @"xrknxpje9j58kbzag5zpw8c63ma8sghs",
                                       @"token" : @"aff62umj5pzqpvam",
                                       @"locale" : @"en-US",
                                       @"flow" : @"standard",
                                       @"form" : @"photoManager_upload_photo_form",
                                       @"capture_screen" : @"editProfile",  // screen or capture_screen?
                                       @"photo_submit" : @"upload",
                                       @"response_type" : @"token",
                                       @"flow_version" : @"737d5aa7-7cda-4ce5-879e-55b5c216505d",
                                       }];
    
    // Random transaction ID
    NSString *randomString = [self randomTransactionID];
    params[@"capture_transactionId"] = randomString;
    NSLog(@"Transaction ID for photo upload: %@", randomString);
    self.textView.text = [NSString stringWithFormat:@"Uploading image with transactionId: %@", randomString];
    
    // Boundary
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSData *boundaryData = [[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding];
    NSData *closeForm = [[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding];
    
    // Content-Type
    NSString *contentTypeString = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentTypeString forHTTPHeaderField:@"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    for (NSString *key in params.allKeys) {
        [body appendData:boundaryData];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", key] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", params[key]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    // Image Data
    NSData *imageData = UIImageJPEGRepresentation(image, 0.9);
    [body appendData:boundaryData];
    [body appendData:[[NSString stringWithFormat:
                       @"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n\r\n", @"photo"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:imageData];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:closeForm];
    
    [request setHTTPBody:body];
    
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)body.length] forHTTPHeaderField:@"Content-Length"];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (!error) {
            NSLog(@"Success.");
            NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSLog(@"Returned data: %@", responseString);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                self.textView.text = [NSString stringWithFormat:@"Success.\n%@\nDownloading image at http://mundstein.at/upload/image.jpg into web view below.", responseString];
                [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://mundstein.at/upload/image.jpg"]]];
            });
        }
        else {
            NSLog(@"%@", error);
            dispatch_async(dispatch_get_main_queue(), ^{
                self.textView.text = error.description;
            });
        }
    }];
    [task resume];

}


@end
