## File Upload Demo ##

Simply run this demo to see what parameters are sent to the server. The photo upload is also verified by re-downloading it and displaying it in a web view. 

*@ Sascha Mundstein 2015*   
*MIT License*
